#!/usr/bin/env bash

php bin/console doctrine:database:drop --force --if-exists
php bin/console doctrine:database:create -v
php bin/console doctrine:schema:update --force
php bin/console doctrine:fixtures:load




