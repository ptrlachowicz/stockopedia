<?php

namespace App\DataFixtures;

use App\Entity\Fact;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class FactFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i < 11; $i++) {
            for ($j = 1; $j < 11; $j++) {
                $security = $manager->getRepository('App\Entity\Security')->find($i);
                $attribute = $manager->getRepository('App\Entity\Attribute')->find($j);

                $fact = new Fact();
                $fact->setAttribute($attribute)
                    ->setSecurity($security)
                    ->setValue($i * $j);

                 $manager->persist($fact);
            }
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            AttributeFixtures::class,
            SecurityFixtures::class
        ];
    }
}
