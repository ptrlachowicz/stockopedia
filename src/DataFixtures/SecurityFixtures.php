<?php

namespace App\DataFixtures;

use App\Entity\Security;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SecurityFixtures extends Fixture
{
    const SYMBOLS = [
        'ABC',
        'BCD',
        'CDE',
        'DEF',
        'EFG',
        'FGH',
        'GHI',
        'HIJ',
        'IJK',
        'JKL',
    ];

    public function load(ObjectManager $manager): void
    {
        foreach (self::SYMBOLS as $symbol) {
            $entity = new Security();
            $entity->setSymbol($symbol);

            $manager->persist($entity);
        }

        $manager->flush();
    }
}
