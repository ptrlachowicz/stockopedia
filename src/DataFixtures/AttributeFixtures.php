<?php

namespace App\DataFixtures;

use App\Entity\Attribute;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AttributeFixtures extends Fixture
{
    const ATTRIBUTES = [
        'price',
        'eps',
        'dps',
        'sales',
        'ebitda',
        'free_cash_flow',
        'assets',
        'liabilities',
        'debt',
        'shares',
    ];

    public function load(ObjectManager $manager): void
    {
        foreach (self::ATTRIBUTES as $attribute) {
            $entity = new Attribute();
            $entity->setName($attribute);

            $manager->persist($entity);
        }

        $manager->flush();
    }
}
