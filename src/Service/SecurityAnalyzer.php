<?php

namespace App\Service;

use App\Entity\Fact;
use App\Entity\Security;
use Doctrine\ORM\EntityManagerInterface;

class SecurityAnalyzer
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function processQuery(array $query): float
    {
        $security = $this->em->getRepository(Security::class)->findOneBy(['symbol' => $query['security']]);

        return $this->evaluateExpr($query['expression'], $security);
    }

    private function evaluateExpr(array $expression, Security $security): float
    {
        if (is_string($expression['a'])) {
            $a = $this->getValueForAttributeAndSecurity($expression['a'], $security);
        } elseif (is_integer($expression['a'])) {
            $a = $expression['a'];
        } else {
            $a = $this->evaluateExpr($expression['a'], $security);
        }

        if (is_string($expression['b'])) {
            $b = $this->getValueForAttributeAndSecurity($expression['b'], $security);
        } elseif (is_integer($expression['b'])) {
            $b = $expression['b'];
        } else {
            $b = $this->evaluateExpr($expression['b'], $security);
        }

        return $this->calc($a,$b, $expression['fn']);
    }

    private function calc(float $a, float $b, $operator): float
    {
        switch ($operator) {
            case '*':
                return $a * $b;
            case '/':
                return $a / $b;
            case '+':
                return $a + $b;
            case '-':
                return $a - $b;
        }
    }

    private function getValueForAttributeAndSecurity(string $attribute, Security $security): float
    {
        $fact =  $this->em->getRepository(Fact::class)
            ->createQueryBuilder('f')
            ->join('f.attribute', 'atr')
            ->andWhere('f.security = :security')
            ->andWhere('atr.name = :name')
            ->setParameter('security', $security)
            ->setParameter('name', $attribute)
            ->getQuery()
            ->getOneOrNullResult();

        return $fact->getValue();
    }
}