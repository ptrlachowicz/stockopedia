<?php

namespace App\Controller;

use App\Service\SecurityAnalyzer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StatsController extends AbstractController
{
    /**
     * @Route("/stats", methods={"POST"}, name="stats")
     */
    public function index(Request $request, SecurityAnalyzer $securityAnalyzer): Response
    {
        $query = json_decode($request->getContent(), true);
        $data = $securityAnalyzer->processQuery($query);

        return $this->json([
            'data' => $data,
        ]);
    }
}
