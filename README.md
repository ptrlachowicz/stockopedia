Install dependencies
```
composer install
```

Run script in order to fill up db with data. Just make sure you have created db and specified credentials in env file
```
./loadData
```

Make post request to localhost/stats endpoint with body in json format

```
curl -X POST http://127.0.0.1:8000/stats -H 'Content-Type: application/json' -d 
'{
"expression": {
"fn": "-",
"a": {"fn": "-", "a": "eps", "b": "shares"},
"b": {"fn": "-", "a": "assets", "b": "liabilities"}
},
"security": "CDE"
}'
```